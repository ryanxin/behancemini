import grace from "../grace/index.js"

//seting http

grace.http.config.baseURL = 'http://www.behance.net/v2/';
grace.http.config.timeout = 5000;
grace.http.interceptors.request.use((con) => {
  getCurrentPages()[getCurrentPages().length -1].setData({showLoading : true})
  // grace.http.owner.$data.showLoading = true

  console.log("interceptors.request", con);
  con.body.api_key = "Fm1jx6RHc0Pk3AYeYpWgD8x9qpBa4pPU";
  //   con.body = JSON.stringify(con.body)
  return con;
});

grace.http.interceptors.response.use(
  (response) => {
    console.log(response);
    getCurrentPages()[getCurrentPages().length -1].setData({showLoading : false})
    // grace.http.owner.$data.showLoading = false
    return response.data
  },
  (err) => {
    console.log(err);
    getCurrentPages()[getCurrentPages().length -1].setData({showLoading : false})
    // grace.http.owner.$data.showLoading = false

    // wx.showModal({
    //   title: '提示',
    //   content: err.message,
    //   showCancel: false,
    // });
    wx.showToast({
      title: err.message,
      icon: ''
    });
    // Do something with response error
    //return Promise.resolve("ssss")
  }
);

// setting base page
var page = grace.page;
grace.page = function (ob) {
  // console.log(page)
  // console.log(grace)
  grace.mixin(ob, {
    onLoad() {
      grace.http.owner = this;
      //页面调用onShow时打印出当前页面id 
      console.log("onLoad, pageID:" + this.$id)
    },
    onShow() {
      //页面调用onShow时打印出当前页面id     
      console.log(this.__wxExparserNodeId__);

      console.log("onShow, pageID:" + this.$id)
    }
  })
  // grace.page.T = getApp().T;
  //创建页面
  page.call(grace, ob)
}

export default grace;