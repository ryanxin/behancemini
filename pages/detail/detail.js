//index.js
//获取应用实例
import grace from "../../common/base.js"
import * as be from '../../network/api.js'

const app = getApp()
const _ = app.T._

grace.page({
  timeOut: null,
  data: {
    modules: [],
    hello1: _('Hello')
  },
  //事件处理函数
  onLoad: function (option) {
    console.log(option)
    be.project_detailed(option.id).then((d) => {
      console.log(d.project.modules)
      this.$data.modules = d.project.modules
    });
  }
});