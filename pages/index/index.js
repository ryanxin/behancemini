//index.js
//获取应用实例
import grace from "../../common/base.js"
import be from '../../network/api.js'

const app = getApp()
const _ = app.T._

grace.page({
  mPage: 1,
  mSearch: "",
  timeOut: null,
  be1: new be(this),
  data: {
    collections: [],
    hello1: _('Hello')
  },
  getProjectList: function (needReload = true) {
    if (needReload) {
      this.mPage = 0;
    }
    this.be1.project(++this.mPage, this.mSearch).then((result) => {
      if (needReload) {
        this.$data.collections = result.projects;
      } else {
        this.$data.collections = this.$data.collections.concat(result.projects)
      }
    })
  },
  //事件处理函数
  bindKeyInput: function (e) {
    var that = this;
    if (this.timeOut != null) {
      clearTimeout(this.timeOut);
    }
    this.timeOut = setTimeout(function () {
      that.mSearch = e.detail.value;
      that.getProjectList();
    }, 400);
  },

  onRefresh: function () {
    console.log(this.__wxExparserNodeId__)
    this.getProjectList();
  },

  onLoadMore: function () {
    this.getProjectList(false);
  },

  onLoad: function () {
    this.getProjectList();
  }
});