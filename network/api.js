import grace from "../common/base.js"
// console.log(grace)
// grace.http.config.baseURL = 'http://www.behance.net/v2/'
// grace.http.config.timeout = 5000;
// grace.http.interceptors.request.use((con) => {
//   console.log("interceptors.request",con);
//   con.body = JSON.stringify(con.body)
//   return con;
// });
const net = grace.http;
export default class BeApi {
    constructor(page) {
        this.page = page;
      }

    project(pg, key = "") {
        return net.get("projects", {
            page: pg,
            q: key
        });
    }

    project_detailed(id) {
        return net.get("projects/" + id, {});
    }

    collection(key = "") {
        return net.get("collections", {
            q: key
        });
    }
}



// export function collection.search(id) {
//     return net.get("collections",{id: id});
// }

// export function collection(key) {
//     return net.get("collections",{q: key});
// }

// export function collection(key) {
//     return net.get("collections",{q: key});
// }

// export function collection(key) {
//     return net.get("collections",{q: key});
// }