let T = {}
T.locale = 0
T.locales = {}

T.registerLocale = function (locales) {
    T.locales = locales;
}

T.setLocale = function (code) {
    switch (code) {
        case "en":
            T.locale = 1;
            break;
        default:
        case "cn":
            T.locale = 0;
    }
}

T._ = function (line, data) {
    const locale = T.locale
    const locales = T.locales
    if (locales[line] && locales[line][locale]) {
        line = locales[line][locale]
    }

    return line
}

export default T